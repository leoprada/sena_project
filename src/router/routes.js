
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', name: 'login', component: () => import('pages/login.vue') },
      { path: 'index', name: 'index', component: () => import('pages/Index.vue') },
      { path: 'service', name: 'serviceList', component: () => import('pages/serviceList.vue') },
      { path: 'service/:id', name: 'service', component: () => import('pages/service.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
